<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Alta Modelo Vehículo</title>
</head>
<body>
   <?php

    require_once("Marca_Modelo.php");

    $var=new Marca_Modelo();
    $mat=$var->getMarca_vehiculo();
   
   $cad=implode(":",$mat);
   $reemp=str_replace(";","",$cad);
   $cadena_vector=explode(" ",$reemp);
   $long_vector=count($cadena_vector);
   
   for ($k=0;$k<$long_vector;$k++) {
       if ($cadena_vector[$k]=="") {
           unset($cadena_vector[$k]);
       }
   }
   $array_d=  array_values($cadena_vector);
   
   ?>
<div align="center">
<h1>Datos del Modelo</h1>
<hr>

<p>Alta Modelo de Vehículo</p>

<table border="1">

    <form action="Alta_Modelo.php" method="post">

<tr><td>Id:</td><td><input type="text" name="id"></td></tr>
<tr><td>Modelo:</td><td><input type="text" name="modelo"></td></tr>
<tr><td>Motor:</td><td><input type="text" name="motor"></td></tr>
<tr><td>Marca:</td><td>
    <?php
       
    $vector_marcas=$array_d;
    $longitud_vector=count($array_d);
    $nombre="marcas";
    $resultado=lista_marcas($nombre, $vector_marcas);
    echo $resultado;
    
    //FUNCION QUE COGE LOS DATOS DE LOS VEHÍCULOS Y LOS MUESTRA EN UN COMBOBOX
    function lista_marcas($nombre,$marcas) {
    $vector_marcas=$marcas;
    $longitud_vector=count($vector_marcas);
    $txt="<select name='$nombre' id='$nombre'>";
    for($i=0;$i<$longitud_vector;$i++) {
        if($i%2!=0) {
            
        $txt.="<option value='$i'>".$vector_marcas[$i].'</option>';
        }
    }
    $txt.='</select>';
    return $txt;
}
    ?>   
        </td></tr>
        </select></td></tr>

<tr><td><input type="submit" name="enviar" value="Enviar"></td>
<td><input type="reset" name="borrar" value="Borrar"></td></tr>
<?php

error_reporting(0);
include_once("config.php");
include_once ('ModeloV_Modelo.php');

//COGE DATOS DEL FORMULARIO Y COMPRUEBA QUE LA CADENA NO ESTÉ VACÍA Y QUE NO HAYA ESPACIOS
if (($_REQUEST['id']!="" && $_REQUEST['modelo']!="" && $_REQUEST['motor']!="")) {
    
        $tmp_id = (isset($_REQUEST['id'])) ? strip_tags(trim(htmlspecialchars($_REQUEST['id'], ENT_QUOTES, "ISO-8859-1"))) : "";
$id_form=str_replace(" ","",(trim($tmp_id)));

    $tmp_modelo = (isset($_REQUEST['modelo'])) ? strip_tags(trim(htmlspecialchars($_REQUEST['modelo'], ENT_QUOTES, "ISO-8859-1"))) : "";
$modelo_form=str_replace(" ","",(trim($tmp_modelo)));


    $tmp_motor = (isset($_REQUEST['motor'])) ? strip_tags(trim(htmlspecialchars($_REQUEST['motor'], ENT_QUOTES, "ISO-8859-1"))) : "";
$motor_form=str_replace(" ","",(trim($tmp_motor)));
} else if ($_REQUEST['id']==""){
    
    echo "<p>Introduce algún valor en el id por favor. </p>";
    
} else if ($_REQUEST['modelo']=="") {
    echo "<p>Introduce algún valor en el modelo del vehículo por favor. </p>";
} else if($_REQUEST['motor']=="") {
    echo "<p>Introduce algún valor en el motor del vehículo por favor. </p>";
}
$tmp_marcaModelo=$_REQUEST['marcas'];
$marcas_form=  str_replace(" ","",(trim($tmp_marcaModelo)));


$modelo=new ModeloV_Modelo();
$modelo->setId_modelo($id_form);
$modelo->setModelo_vehiculo($modelo_form);
$modelo->setModelo_motor($motor_form);

$modelo->setModelo_marca($array_d[$marcas_form-1]);

?>

</form>
</table>
<a href="index.php">Inicio</a>
<p>&copy; <?php echo AUTOR." ".FECHA." "." ".EMPRESA." ".CURSO ?></p>
<hr>
</div>
</body>
</html>
